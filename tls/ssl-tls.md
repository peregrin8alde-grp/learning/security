# SSL/TSL

SSL/TLS 認証関連技術について

## 規約／標準化

- TLS 1.3 : https://tools.ietf.org/html/rfc8446
- TLS 1.2
  - 英語: https://tools.ietf.org/html/rfc5246
  - 日本語訳: https://www.ipa.go.jp/security/rfc/RFC5246-00JA.html
- TLS 1.1 : https://tools.ietf.org/html/rfc4346
- TLS 1.0 : https://tools.ietf.org/html/rfc2246
- 参考: https://www.ipa.go.jp/security/rfc/RFC.html#15

## 実装ライブラリ

SSL は既に古く安全ではないため、基本 TLS をサポートしている。

- OpenSSL: https://www.openssl.org/
- GnuTLS: https://www.gnutls.org/
- wolfSSL
  - https://www.wolfssl.com/
  - 日本語: https://www.wolfssl.jp/

## プロトコル概要

1. クライアント：サーバに接続要求
1. サーバ：サーバ証明書（実在証明などのための情報と自身の公開鍵）をクライアントに送付
   - ルートまたは中間認証局によってサーバ証明書に署名を受けることで実在証明などを行えるようにする
1. クライアント：サーバ証明書が信用できるかどうかを確認
   - ルート証明書を使って確認
1. クライアント：サーバと暗号化通信するための共通鍵を用意
1. クライアント：サーバ証明書に含まれるサーバ公開鍵で共通鍵を暗号化、サーバに送付
1. サーバ：自身の秘密鍵で共通鍵を復号
1. サーバ／クライアント：共通鍵で暗号化通信

## 認証局（CA）

- ルート認証局: 自身の証明書への署名を行える認証局。
  - パブリック認証局: 公的に信頼された認証局。OS にデフォルトで証明書がインストールされている。
  - プライベート認証局: 社内用など、独自に運用されている認証局。個別に証明書のインストールが必要。
    - 個人が作成した認証局（所謂オレオレ認証局／証明書）はリスクが高いため機能検証など以外で使うべきではない。
      （クライアント側が証明書を信頼するかどうか判断する単位が会社＝＞個人となり、作業が煩雑なために管理がいい加減になる）
- 中間認証局: ルート認証局によって信頼された認証局。中間／サーバ証明書への署名に使われる。
  - ルート認証局でサーバ証明書へ直接署名するよりも管理／運用が楽になる。
