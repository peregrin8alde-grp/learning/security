# HTTPS 動作確認用 サンプルサーバ

自作したサーバ証明書の動作確認を行うため、HTTPS 接続確認用のサーバを構築する。

## セットアップ

- CA は既に作っている `../ca/demoCA/` を利用する想定
- 分かりやすくするために、`openssl.cnf` は専用のものを用意
  - `[ req_distinguished_name ]` に `commonName_default		= 192.168.3.5` 追加

```
mkdir -p ./tls

cp -f ../ca/CA.pl ./tls/
cp -f ../ca/openssl.cnf ./tls/
```

```
cd ./tls

export OPENSSL_CONFIG="-config openssl.cnf"

./CA.pl -newreq-nodes

openssl req -text -noout -in newreq.pem

cp -p newreq.pem ../../ca/
```

```
cd ../../ca

./CA.pl -revoke newcert.pem

./CA.pl -sign

cp -f newcert.pem ../sampleSv/tls

cd ../sampleSv
```
