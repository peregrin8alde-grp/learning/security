# HTTPS 動作確認用 Nginx サーバ

Nginx + Docker で HTTPS 接続確認用のサーバを構築する。

- http://nginx.org/en/docs/http/configuring_https_servers.html
- https://hub.docker.com/_/nginx

## 起動／再起動／停止／削除
### HTTPS なし

```
docker run \
  --name nginx \
  -d \
  -p 8080:80 \
  -v $(pwd)/html:/usr/share/nginx/html:ro \
  -v $(pwd)/nginx-conf/nginx.conf:/etc/nginx/nginx.conf:ro \
  nginx

docker restart nginx

docker stop nginx
docker rm -v nginx
```

http://192.168.3.5:8080

### HTTPS あり

```
docker run \
  --name nginx-tls \
  -d \
  -p 443:443 \
  -v $(pwd)/html:/usr/share/nginx/html:ro \
  -v $(pwd)/nginx-conf/nginx-tls.conf:/etc/nginx/nginx.conf:ro \
  -v $(pwd)/tls:/etc/nginx/tls \
  nginx

docker restart nginx-tls

docker stop nginx-tls
docker rm -v nginx-tls
```

https://192.168.3.5

- ルート証明書をインストールしていない場合、`SEC_ERROR_UNKNOWN_ISSUER` エラーが発生する。
  FireFox の場合は以下の方法でルート証明書をインストール可能。
  
  1. [編集] - [設定] - [プライバシーとセキュリティ](about:preferences#privacy)
  2. [証明書を表示] - [認証局証明書] - [インポート]
  3. `demoCA/cacert.pem` をインストールする

