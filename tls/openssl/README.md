# OpenSSL

- https://www.openssl.org/

## 認証局

- ラッパーツールを利用すると楽に管理できる。

  - https://www.openssl.org/docs/faq.html

    > Q: How do I create certificates or certificate requests?  
    > A: Check out the CA.pl(1) manual page. This provides a simple wrapper round  
    > the 'req', 'verify', 'ca' and 'pkcs12' utilities. For finer control check out  
    > the manual pages for the individual utilities and the certificate extensions  
    > documentation (in ca(1), req(1), and x509v3_config(5) ).

  - https://www.openssl.org/docs/manmaster/man1/CA.pl.html
  - Ubuntu の場合は `/usr/lib/ssl/misc/` にあった。簡略化のために `cp -rpf /usr/lib/ssl/misc/CA.pl ./`
  - `./demoCA` というディレクトリ上に各種ファイルが作成される。
  - デフォルトでは `/usr/lib/ssl/openssl.cnf` が使われる。（`OPENSSL_CONFIG` で指定方法を変更可能）
  - 最低でも以下は入力しないとエラー
    - `PEM pass phrase`
    - `Common Name (e.g. server FQDN or YOUR name) []`
    - `pass phrase for ./demoCA/private/cakey.pem`

Complete certificate creation example: create a CA, create a request, sign the request and finally create a PKCS#12 file containing it.

### CA 作成

CAとして動くための各種ディレクトリ／ファイル構成の構築

- `./demoCA` というディレクトリ（`$CATOP`指定値）に出力
- 最初の CA 証明書のファイル名指定を省略した場合、`cacert.pem` というファイル名（ `$CACERT` 指定値）で CA 証明書を新規生成
  1. 署名要求（秘密鍵 `private/cakey.pem` 、署名要求 `careq.pem` を実施
  1. `v3_ca` セクションの設定を使って自己署名

```
cp -f /usr/lib/ssl/openssl.cnf ./openssl.cnf
export OPENSSL_CONFIG="-config openssl.cnf"

rm -rf ./demoCA

./CA.pl -newca
```

内部処理関連メッセージ

```
openssl req  -new -keyout ./demoCA/private/cakey.pem -out ./demoCA/careq.pem
Generating a RSA private key
writing new private key to './demoCA/private/cakey.pem'

openssl ca  -create_serial -out ./demoCA/cacert.pem -days 1095 -batch -keyfile ./demoCA/private/cakey.pem -selfsign -extensions v3_ca  -infiles ./demoCA/careq.pem
Using configuration from openssl.cnf

CA certificate is in ./demoCA/cacert.pem
```

### 署名要求作成

CAに対してサーバ証明書への署名要求を作成。

- 署名を受けたいサーバ側で実施。
  - ルート CA での自己署名用の要求は `-newcert` の方（ `-x509` が追加される）？
- `newkey.pem` というファイル名（ `$NEWKEY` 指定値）で秘密鍵を新規生成
  - `-newreq-nodes` の場合はパスワードで暗号化しない（作成／利用時にパスワードが聞かれない）。
- `newreq.pem` というファイル名（ `$NEWREQ` 指定値）で署名要求を新規生成
- 中で `-days` オプションが指定されているが、自己署名用（`-x509` 指定付き）じゃないと無視される？

```
./CA.pl -newreq
```

内部処理関連メッセージ

```
openssl req  -new  -keyout newkey.pem -out newreq.pem -days 365
Ignoring -days; not generating a certificate
Generating a RSA private key
writing new private key to 'newkey.pem'

Request is in newreq.pem, private key is in newkey.pem
```

```
./CA.pl -newreq-nodes
```

```
openssl req -config openssl.cnf -new -nodes -keyout newkey.pem -out newreq.pem -days 365 
Ignoring -days; not generating a certificate
Generating a RSA private key
writing new private key to 'newkey.pem'

Request is in newreq.pem, private key is in newkey.pem
```

### 署名

CAによるサーバ証明書への署名を行う。

- `newreq.pem` というファイル名（ `$NEWREQ` 指定値）の署名要求が入力
- `newcert.pem` というファイル名（ `$NEWCERT` 指定値）で署名済みサーバ証明書を出力
- 署名時には CA 作成時に作成／指定した秘密鍵を利用
- 設定ファイルの `policy_anything` セクションを利用

```
./CA.pl -sign
```

内部処理関連メッセージ

```
openssl ca  -policy policy_anything -out newcert.pem  -infiles newreq.pem
Using configuration from openssl.cnf

Signed certificate is in newcert.pem
```

### PKCS#12 への変換

秘密鍵をパスワードで保護しつつ、中間／サーバ証明書とセットでまとめた p12 ファイルへと変換する。

- 鍵や証明書をまとめてインポート／エクスポートするのに利用？

```
./CA.pl -pkcs12 "My Test Certificate"
```

内部処理関連メッセージ

```
openssl pkcs12 -in newcert.pem -inkey newkey.pem -certfile ./demoCA/cacert.pem -out newcert.p12 -export -name "My Test Certificate"

PKCS #12 file is in newcert.p12
```
