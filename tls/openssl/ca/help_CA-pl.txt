usage: CA.pl -newcert | -newreq | -newreq-nodes | -xsign | -sign | -signCA | -signcert | -crl | -newca [-extra-cmd extra-params]
       CA.pl -pkcs12 [-extra-pkcs12 extra-params] [certname]
       CA.pl -verify [-extra-verify extra-params] certfile ...
       CA.pl -revoke [-extra-ca extra-params] certfile [reason]
