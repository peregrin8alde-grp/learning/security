import React from "react";

type Props = {
  onClick(event: React.MouseEvent<HTMLButtonElement>): void;
};

function SignOut(props: Props) {
  return <button onClick={props.onClick}>SignOut</button>;
}

export default SignOut;
