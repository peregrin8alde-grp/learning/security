import React from "react";

type Props = {
  children: React.ReactNode;
};

function Dashboard(props: Props) {
  return (
    <div>
      <h1>Dashboard</h1>

      {props.children}
    </div>
  );
}

export default Dashboard;
