import React, { useState, useEffect } from 'react';
import Dashboard from "./components/dashboard/Dashboard";
import SignOut from "./components/SignOut";

function App() {
  const [token, setToken] = useState({});

  useEffect(() => {
    fetch("/oauth/token", {
      method: "GET",
    }).then(
      (result) => {
        //console.log(result);
        if (result.ok) {
          result.headers.forEach((value, key) => {
            console.log(key + " : " + value);
          });

          return result.json();
        }
      },
      (error) => {
        console.log(error);
      }
    ).then(
      (json) => {
        console.log(json);

        setToken(json);
      },
      (error) => {
        console.log(error);
      }
    );
  }, []);  // 無限ループ防止のために、更新監視変数として空配列指定

  const signOut = (event: React.MouseEvent<HTMLButtonElement>): void => {
    //event.preventDefault();

    fetch("/oauth/logout", {
      method: "GET",
    }).then(
      (result) => {
        console.log(result);
        if (result.ok) {
          console.log("signOut");

          //window.location.reload(); // react では location という名前をグローバルには使えないので、 window まで付ける
          window.location.href = "/public";
        }
      },
      (error) => {
        console.log(error);
      }
    );
  };

  return (
    <div>
      <Dashboard>
        <SignOut onClick={(e) => signOut(e)}></SignOut>
      </Dashboard>
    </div>
  );
}

export default App;
