# 動作確認用 Web クライアント

- 認証クライアント（RP） ではなく、Web アプリとしてのクライアント想定
- React などのような SPA にする場合、処理フローやセキュリティーについて別途注意が必要

```
docker run \
  --rm \
  -it \
  -u "node" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app" \
  node \
    npx create-react-app appclient --template typescript

docker run \
  --rm \
  -it \
  --name myapp-typescript \
  -p 13000:3000 \
  -u "node" \
  -v $(pwd)/appclient:/home/node/app \
  -w "/home/node/app" \
  node \
    yarn start
```

http://localhost:13000

```
rm -rf appclient/build \
&& docker run \
  --rm \
  -it \
  --name myapp-typescript \
  -u "node" \
  -v $(pwd)/appclient:/home/node/app \
  -w "/home/node/app" \
  node \
    yarn build \
&& rm -rf ../mock/static/secret \
&& cp -rp appclient/build ../mock/static/secret
```
