# TLS 設定

- https://www.keycloak.org/docs/latest/server_installation/index.html#_setting_up_ssl
- https://www.keycloak.org/docs/latest/server_installation/index.html#enabling-ssl-https-for-the-keycloak-server
- https://github.com/keycloak/keycloak-containers/blob/11.0.0/server/README.md#setting-up-tlsssl

## 証明書作成

https://www.keycloak.org/docs/latest/server_installation/index.html#self-signed-certificate

- 作成済みの秘密鍵とサーバ証明書を利用
  - 参照 : https://gitlab.com/peregrin8alde-grp/learning/security/-/tree/master/tls/openssl/sampleSv
  - 秘密鍵の暗号化（パスワード設定）を行っていると上手く行くか不明。
- Java Keystore : Java が SSL/TLS で必要となる秘密鍵／証明書を扱うための仕組み
  - Docker イメージを利用する場合、 `/etc/x509/https` に配置されたものを自動で Keystore に登録

```
mv -f newcert.pem tls.crt
mv -f newkey.pem tls.key
```

## 起動

```
BASE_DIR=$(cd .. && pwd)

mkdir -p ${BASE_DIR}/backup

docker run \
  --name keycloak \
  --network=keycloak-net \
  -it \
  -p 8080:8080 \
  -p 8443:8443 \
  -v ${BASE_DIR}/tls:/etc/x509/https \
  -v ${BASE_DIR}/backup:/opt/jboss/keycloak/standalone/data/backup \
  -e KEYCLOAK_USER=admin \
  -e KEYCLOAK_PASSWORD=admin \
  quay.io/keycloak/keycloak:10.0.2
```

https://192.168.3.5:8443/auth/admin

## 利用

```
export PROXY_CLIENT_ID=gatekeeper
export PROXY_CLIENT_SECRET=[secret]

BASE_DIR=$(cd .. && pwd)

docker run \
  --name gatekeeper \
  -it \
  --rm \
  -p 3000:3000 \
  --network=keycloak-net \
  -v ${BASE_DIR}/config:/config \
  -v ${BASE_DIR}/templates:/templates \
  -v /etc/ssl/certs/ca-certificates.crt:/etc/ssl/certs/ca-certificates.crt:ro \
  quay.io/louketo/louketo-proxy \
    --config /config/common.yml \
    --discovery-url=https://192.168.3.5:8443/auth/realms/myrealm \
    --client-id=${PROXY_CLIENT_ID} \
    --client-secret=${PROXY_CLIENT_SECRET}
```


## トラブルシューティング

- 起動時にエラー

  ```
  java.io.IOException: keystore password was incorrect
  ```

  - 関連メッセージ

    ```
    Creating HTTPS keystore via OpenShift's service serving x509 certificate secrets..
    HTTPS keystore successfully created at: /opt/jboss/keycloak/standalone/configuration/keystores/https-keystore.jks
    〜中略〜
    08:45:30,069 ERROR [org.jboss.msc.service.fail] (MSC service thread 1-8) MSC000001: Failed to start service org.wildfly.security.key-store.kcKeyStore: org.jboss.msc.service.StartException in service org.wildfly.security.key-store.kcKeyStore: WFLYELY00004: Unable to start the service.
            at org.wildfly.extension.elytron@11.1.1.Final//org.wildfly.extension.elytron.KeyStoreService.start(KeyStoreService.java:171)
    〜中略〜
    Caused by: java.io.IOException: keystore password was incorrect
            at java.base/sun.security.pkcs12.PKCS12KeyStore.engineLoad(PKCS12KeyStore.java:2108)
            at java.base/sun.security.util.KeyStoreDelegator.engineLoad(KeyStoreDelegator.java:243)
    〜中略〜
    Caused by: java.security.UnrecoverableKeyException: failed to decrypt safe contents entry: javax.crypto.BadPaddingException: Given final block not properly padded. Such issues can arise if a bad key is used during decryption.
            ... 14 more
    〜中略〜
    08:45:30,249 WARN  [org.jboss.as.domain.management.security] (MSC service thread 1-3) WFLYDM0111: Keystore /opt/jboss/keycloak/standalone/configuration/application.keystore not found, it will be auto generated on first use with a self signed certificate for host localhost
    〜中略〜
    08:45:42,648 ERROR [org.jboss.as.controller.management-operation] (Controller Boot Thread) WFLYCTL0013: Operation ("add") failed - address: ([
        ("subsystem" => "elytron"),
        ("key-store" => "kcKeyStore")
    ]) - failure description: {"WFLYCTL0080: Failed services" => {"org.wildfly.security.key-store.kcKeyStore" => "WFLYELY00004: Unable to start the service.
        Caused by: java.io.IOException: keystore password was incorrect
        Caused by: java.security.UnrecoverableKeyException: failed to decrypt safe contents entry: javax.crypto.BadPaddingException: Given final block not properly padded. Such issues can arise if a bad key is used during decryption."}}
    ```

  - `keytool` 単体実行でも同様のエラー

    ```
    $ tail -n 2 ${JBOSS_HOME}/bin/.jbossclirc
    set keycloak_tls_keystore_password=wb8JiR727dfTPkaBswd0BCAJFTic8a4Gn2FWMC1W2Pw=
    set keycloak_tls_keystore_file=/opt/jboss/keycloak/standalone/configuration/keystores/https-keystore.jks

    $ keytool -list -v -storepass "wb8JiR727dfTPkaBswd0BCAJFTic8a4Gn2FWMC1W2Pw=" -keystore /opt/jboss/keycloak/standalone/configuration/keystores/https-keystore.jks
    keytool error: java.io.IOException: keystore password was incorrect
    java.io.IOException: keystore password was incorrect
    〜中略〜
    Caused by: java.security.UnrecoverableKeyException: failed to decrypt safe contents entry: javax.crypto.BadPaddingException: Given final block not properly padded. Such issues can arise if a bad key is used during decryption.
    ```
  
  - `-v keycloak:/opt/jboss/keycloak` での永続化をしなければ発生しないため、何らかのゴミが影響している可能性がある。
    - なお、新規ボリュームでのコンテナ作成後に何もせず同じボリュームでコンテナを再作成してもエラーは発生しなかった。
    - `/etc/x509/https` へのマウントを外してもエラーが治らなかった。Realm 設定で SSL を常時要求に設定したためか？
    - `/opt/jboss/keycloak/standalone/configuration/standalone.xml` / `standalone-ha.xml` 内に `<tls>` / `<key-stores>` というタグがあり、そこに古いパスワードでの情報が残っている模様。
    - `<https-listener>` も影響している模様。
    - なお、上記２箇所以外に `standalone.xml` / `standalone-ha.xml` に違いはなさそう。（初回起動直後と比較）
    - 上記２ファイル／２箇所を初回起動直後状態に戻すと、エラーは発生しなくなった。
  - どうも `/etc/x509/https` を新しい鍵に入れ替えても更新されない模様？
    - `.jbossclirc` に出力されている一番最初のパスワードが通った。
    - keystore 自体は以下で削除できるが、 keycloak 側の設定は残る？
      - `keytool -delete -v -alias keycloak-https-key -storepass LuVi9ek9B2yXjsA+RKZFKKDYmHACAgL4obw3R4/zLDw= -keystore /opt/jboss/keycloak/standalone/configuration/keystores/https-keystore.jks`
- [未解決] ブラウザで開いた際に `MOZILLA_PKIX_ERROR_SELF_SIGNED_CERT`

  ```
  https://192.168.3.5:8443/auth/admin

  The certificate is not trusted because it is self-signed.
  HTTP Strict Transport Security: false

  HTTP 公開鍵ピンニング: false
  ```

  - ルート証明書をインストールした状態で発生。そもそもマウントした証明書が使われていない模様。
    - 初回起動直後状態だと発生しないため、上記のトラブルが何か影響している可能性がある。
    - エクスポート => 全体マウントなしでインポートでも発生？単に鍵／証明書マウントミスかも？
  - Keycloak 側でも警告

    ```
    13:07:38,225 WARN  [org.jboss.as.domain.management.security] (default I/O-2) WFLYDM0113: Generated self signed certificate at /opt/jboss/keycloak/standalone/configuration/application.keystore. Please note that self signed certificates are not secure, and should only be used for testing purposes. Do not use this self signed certificate in production.
    SHA-1 fingerprint of the generated key is 47:a1:c4:5e:8e:da:be:9e:8e:5f:61:6e:14:3f:fe:81:07:9a:61:22
    SHA-256 fingerprint of the generated key is dd:29:44:01:cb:1b:d2:83:15:fe:f2:3f:20:2a:b5:ff:53:25:af:b5:ec:bd:6a:e6:f7:1a:6e:32:63:d0:6f:b7
    ```
- gatekeeper 起動でエラー `x509: cannot validate certificate for 192.168.3.5 because it doesn't contain any IP SANs`
  - `openssl.cnf` で `subjectAltName` （SAN）が指定されていないため。
    - サーバ側の `req_extensions = v3_req` のコメントアウトを外して `v3_req` セクションで指定、CA 側の `copy_extensions = copy` のコメントアウトを外すことで値をコピー
    - 参考 : https://www.openssl.org/docs/manmaster/man5/x509v3_config.html

    ```
    subjectAltName          = @alt_names

    [alt_names]
    DNS.1 = localhost
    IP.1 = 192.168.3.5
    ```
- gatekeeper 起動でエラー `x509: certificate signed by unknown authority`
  - ルート証明書を登録してないため。
  - ホスト側の信頼済み証明書に登録してマウントするのが手っ取り早い。
    - Ubuntu の場合

      ```
      sudo mkdir -p /usr/share/ca-certificates/private
      sudo cp cacert.pem /usr/share/ca-certificates/private/

      echo private/cacert.pem | sudo tee -a /etc/ca-certificates.conf
      tail -n 1 /etc/ca-certificates.conf

      sudo update-ca-certificates
      ```
    
      - マウント場所は `/etc/ssl/certs/ca-certificates.crt:/etc/ssl/certs/ca-certificates.crt`
