# Gatekeeper

OpenID Connect のクライアント（RP）を担当してくれるプロキシ

参考

- https://github.com/louketo/louketo-proxy
- https://www.keycloak.org/downloads
- https://github.com/louketo/louketo-proxy/releases
- https://github.com/louketo/louketo-proxy/blob/master/docs/user-guide.md#example-of-usage-and-configuration-with-keycloak

## 関連サーバ起動

- リソース用モックとして[json-server](https://github.com/typicode/json-server)を利用。
- SSL/TLS を常に有効化
  - Realm 設定の `Login` - `Require SSL`

```
# ネットワーク追加
docker network create keycloak-net

# モック
docker run \
  --name mock \
  -u "node" \
  -d \
  -p 3000 \
  -v jsonserver:/usr/local \
  -v $(pwd)/mock:/home/node/app \
  -w "/home/node/app" \
  --network=keycloak-net \
  node \
    json-server db.json

# keycloak / TLS なし
docker run \
  --name keycloak \
  -it \
  -p 8080:8080 \
  -v $(pwd)/backup:/opt/jboss/keycloak/standalone/data/backup \
  --network=keycloak-net \
  -e KEYCLOAK_USER=admin \
  -e KEYCLOAK_PASSWORD=admin \
  quay.io/keycloak/keycloak:10.0.2

# keycloak / TLS あり
docker run \
  --name keycloak \
  --network=keycloak-net \
  -it \
  -p 8080:8080 \
  -p 8443:8443 \
  -v $(pwd)/tls:/etc/x509/https \
  -v $(pwd)/backup:/opt/jboss/keycloak/standalone/data/backup \
  -e KEYCLOAK_USER=admin \
  -e KEYCLOAK_PASSWORD=admin \
  quay.io/keycloak/keycloak:10.0.2
```

https://192.168.3.5:8443/auth/admin

## Gatekeeper 用クライアント作成

### Keycloak

- Client ID: `gatekeeper`
- protocol: `openid-connect`
- access-type: `confidential`
- Valid Redirect URIs: `http://127.0.0.1:3000/oauth/callback`
  - 別マシンのブラウザでもアクセスできるように `redirection-url` と合わせて `http://192.168.3.5:3000/oauth/callback` に変更
- Client ID と Credentials の secret を記録しておくこと。
- Role も設定しておく。
  - 作成したクライアント内 Roles で `test1` / `test2` を作成
  - 利用したいユーザの Role Mapping でそれぞれの Role を割当て
- Groups でグループを作成し、ユーザの Gruops を設定しておく。
  - ただ設定しただけでは JWT に含まれないため、マッピングが必要な模様。

### GitLab

- https://docs.gitlab.com/ee/api/oauth2.html
- https://docs.gitlab.com/ee/integration/openid_connect_provider.html
- [アカウントのアプリケーション設定](https://gitlab.com/oauth/applications)で OpenID Connect のための設定が可能。
  - scope は gatekeeper 側がデフォルトで `openid` / `email` / `profile` を使うため、これら全部設定する必要がある。
- https://github.com/doorkeeper-gem/doorkeeper/wiki/API-endpoint-descriptions-and-examples
- https://gitlab.com/.well-known/openid-configuration


## クライアント実行

- `tags` でカスタムサインインページ（`sign-in-page`）用の key=value を追加したい場合、コマンドラインオプションの挙動がおかしい？
- `sign-in-page` を使うより、ログイン前は公開トップページを表示し、そこに要認証のダッシュボードへのリンクを記載、
  ログアウトしたら公開トップページへリダイレクト、としたほうが汎用的で自由度も高い？

### Keycloak

```
export PROXY_CLIENT_ID=[Client ID]
export PROXY_CLIENT_SECRET=[secret]

docker run \
  --name gatekeeper \
  -it \
  --rm \
  -p 3000:3000 \
  --network=keycloak-net \
  -v $(pwd)/config:/config \
  -v /etc/ssl/certs/ca-certificates.crt:/etc/ssl/certs/ca-certificates.crt:ro \
  quay.io/louketo/louketo-proxy \
    --config /config/common.yml \
    --discovery-url=https://192.168.3.5:8443/auth/realms/myrealm \
    --client-id=${PROXY_CLIENT_ID} \
    --client-secret=${PROXY_CLIENT_SECRET}
```

### GitLab

```
export PROXY_CLIENT_ID=[アプリケーション ID]
export PROXY_CLIENT_SECRET=[秘密]

docker run \
  --name gatekeeper \
  -it \
  --rm \
  -p 3000:3000 \
  --network=keycloak-net \
  -v $(pwd)/config:/config \
  -v $(pwd)/templates:/templates \
  quay.io/louketo/louketo-proxy \
    --config /config/common.yml \
    --discovery-url=https://gitlab.com \
    --client-id=${PROXY_CLIENT_ID} \
    --client-secret=${PROXY_CLIENT_SECRET}
```

## 動作確認

- mock
  - admin
    - http://192.168.3.5:3000/admin
    - http://192.168.3.5:3000/admin/1
    - http://192.168.3.5:3000/admin?id=1
  - user
    - http://192.168.3.5:3000/user
    - http://192.168.3.5:3000/user/1
    - http://192.168.3.5:3000/user?id=1
  - backend
    - http://192.168.3.5:3000/backend
    - http://192.168.3.5:3000/backend/1
    - http://192.168.3.5:3000/backend?id=1
  - secret
    - http://192.168.3.5:3000/secret/
  - public
    - http://192.168.3.5:3000/public/
- gatekeeper 機能
  - [logout](https://github.com/louketo/louketo-proxy/blob/master/docs/user-guide.md#logout-endpoint)
    - http://192.168.3.5:3000/oauth/logout?redirect=http://192.168.3.5:8080/auth/realms/myrealm/protocol/openid-connect/logout
  - http://192.168.3.5:3000/oauth/health
  - [その他エンドポイント](https://github.com/louketo/louketo-proxy/blob/master/docs/user-guide.md#endpoints)
    - /oauth/authorize is authentication endpoint which will generate the OpenID redirect to the provider
    - /oauth/callback is provider OpenID callback endpoint
    - /oauth/expired is a helper endpoint to check if a access token has expired, 200 for ok and, 401 for no token and 401 for expired
    - /oauth/health is the health checking endpoint for the proxy, you can also grab version from headers
    - /oauth/login provides a relay endpoint to login via grant_type=password, for example, POST /oauth/login form values are username=USERNAME&password=PASSWORD (must be enabled)
      - `--enable-login-handler` を有効にしておくこと
    - /oauth/logout provides a convenient endpoint to log the user out, it will always attempt to perform a back channel log out of offline tokens
    - /oauth/token is a helper endpoint which will display the current access token for you
    - /oauth/metrics is a Prometheus metrics handler

## トラブルシューティング

- `[error] the cookie is set to secure but your redirection url is non-tls`
  - バックエンドが https ではない場合、`secure-cookie: false`が設定に必要
- コンテナのホスト名での URL（`discovery-url`）にリダイレクトされるため、ブラウザからアクセスできずエラーとなる。
  - `discovery-url` 自体は gatekeeper コンテナからアクセスできるものにしないといけない。
    - ホスト側の IP アドレスならアクセス可能。
  - ブラウザから見える URL（keycloak サーバ自体にアクセスするための URL と同様、別マシンから参照できるもの）に変更。
- `redirection-url` もブラウザが別マシンの場合に同じ問題
  - `discovery-url` 同様に外部からアクセスできる URL を設定
- `'aud' claim and 'client_id' do not match`
  - aud 用の設定が必要らしい。参考：https://qiita.com/k2n/items/635e0b08ecac421c56d2
    > Keycloak に aud クレームの設定を追加する
    >
    > Keycloak Gatekeeper はアクセストークンの検証時に、JWT 内の"aud"クレームと自身のクライアント ID が一致していることを確認する処理があり、これがしばしば大きな問題になります。（というかハマりました。。）
    > 本質的には、アクセストークンは ID トークンと違って特定のクライアントを受け取り手として発行するものではないので、Keycloak が"aud"クレームを含めないようにするか、Keycloak Gatekeeper がこの確認を行わないようにしてくれるのが望ましいはずなのですが、そうなっていません。
    > （Keycloak の issue で議論されていますが、執筆時点では結論はまだのようです）
    >
    > したがって、この問題を回避するために、アクセストークンの"aud"クレームに Keycloak Gatekeeper のクライアント ID をセットする設定を Keycloak に対して追加します。
    >
    >     Keycloakの管理画面で、app-html5クライアントの『マッパー』タブで、『作成』する。
    >     名前を付けて、マッパータイプで『Audience』を選択する。
    >     『Included Client Audience』で『gatekeeper』を選択する。
    >     『アクセストークンに追加』のみONにして、『保存』する。
- `access denied, invalid roles {"access": "denied", "email": "dummy@email.com", "resource": "/backend*", "roles": "client:test1"}`
  - クライアント配下の Roles の場合、`clientid:rolename` となる模様。
    ```
    debug   src/session.go:51       found the user identity {"id": "4ea284e7-223c-4708-9203-3c4d728fe87a", "name": "user1", "email": "user1@dummy.com", "roles": "offline_access,admin,uma_authorization,user,gatekeeper:client:test1,account:manage-account,account:manage-account-links,account:view-profile", "groups": ""}
    ```
- `access denied, invalid groups {"access": "denied", "email": "user1@dummy.com", "resource": "/admin*", "groups": "admins,users"}`
  - keycloak 上で Group に追加しただけでは`groups`として設定されない模様。
  - クライアントの `Mappers` で以下の設定のマッピングを追加
    - `Mapper Type`: `Group Membership`
    - `Token Claim Name`: `groups`
    - `Full group path`: `OFF` （`/admin`などのように`/`が入らないようにするため）
- `src/handlers.go:406 invalid response from revocation endpoint` / `Invalid refresh token`
  - `enable-refresh-tokens: true` に設定する。
