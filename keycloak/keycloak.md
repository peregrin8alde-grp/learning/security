# Keycloak

https://www.keycloak.org/

## get started

- https://www.keycloak.org/getting-started/getting-started-docker
- https://github.com/keycloak/keycloak-containers/blob/10.0.2/server/README.md

### Start Keycloak

- ひとまず DB はデフォルトの `h2` を利用し、 `/opt/jboss/keycloak` 全体を永続化

```
docker run \
  --name keycloak \
  --rm \
  -p 8080:8080 \
  -v keycloak:/opt/jboss/keycloak \
  --network=keycloak-net \
  -e KEYCLOAK_USER=admin \
  -e KEYCLOAK_PASSWORD=admin \
  quay.io/keycloak/keycloak:10.0.2
```

http://localhost:8080/auth/admin

### Create a realm

1. 左のサイドバー最上部の `Master` をマウスオーバーすると `Add realm` と表示されるので、クリック
2. Name: `myrealm`
3. `Create`

### Create a user

1. 左のサイドバー `Users`
2. `Add user`
3. Username: `myuser`, First Name: first, Last Name: last
4. `Save`

ログイン用初期パスワード設定

1. `Credentials` タブ
2. パスワードを設定
3. 初回ログイン時にパスワード変更が必要ない場合は `Temporary` を `OFF`

### Login to account console

http://localhost:8080/auth/realms/myrealm/account

### Secure your first app

http://localhost:8080/auth/admin

1. 左のサイドバー `Clients`
2. `Create`
3. Client ID: `myclient`, Client Protocol: `openid-connect`, Root URL: `https://www.keycloak.org/app/`
4. `Save`
5. https://www.keycloak.org/app/ にアクセス、デフォルト設定で `Save`
6. `Sign in` => 作成したユーザでサインイン

## Client Adapters

Client Adapters を使うことで、OpenID Connect の RP などのような認証クライアント機能を簡単に実装できる。
Web サーバ／クライアントアプリを修正せずに認証機能を実現したい場合、Gatekeeper でプロキシすることで可能。

## Themes

https://www.keycloak.org/docs/latest/server_development/index.html#_themes

ログイン画面やユーザアカウント画面などはテーマを修正することで自作アプリの外観に合わせることが可能。

## Export and Import

- https://www.keycloak.org/docs/latest/server_admin/index.html#_export_import
- 起動に失敗する場合はエクスポートも無理？

```
bin/standalone.sh -Dkeycloak.migration.action=export \
  -Dkeycloak.migration.provider=singleFile \
  -Dkeycloak.migration.file=<FILE TO EXPORT TO>

bin/standalone.sh -Dkeycloak.migration.action=import \
  -Dkeycloak.migration.provider=singleFile \
  -Dkeycloak.migration.file=<FILE TO IMPORT>
  -Dkeycloak.migration.strategy=OVERWRITE_EXISTING
```

### docker

- https://github.com/keycloak/keycloak-containers/blob/11.0.0/server/README.md#importing-a-realm
- https://github.com/keycloak/keycloak-containers/blob/11.0.0/server/README.md#exporting-a-realm

```
docker exec -it kc /opt/jboss/keycloak/bin/standalone.sh \
-Djboss.socket.binding.port-offset=100 -Dkeycloak.migration.action=export \
-Dkeycloak.migration.provider=singleFile \
-Dkeycloak.migration.realmName=my_realm \
-Dkeycloak.migration.usersExportStrategy=REALM_FILE \
-Dkeycloak.migration.file=/tmp/my_realm.json

docker run -e KEYCLOAK_USER=<USERNAME> -e KEYCLOAK_PASSWORD=<PASSWORD> \
    -e KEYCLOAK_IMPORT=/tmp/example-realm.json -v /tmp/example-realm.json:/tmp/example-realm.json jboss/keycloak
```

```
docker exec -it keycloak \
  /opt/jboss/keycloak/bin/standalone.sh \
    -Djboss.socket.binding.port-offset=100 -Dkeycloak.migration.action=export \
    -Dkeycloak.migration.provider=singleFile \
    -Dkeycloak.migration.realmName=myrealm \
    -Dkeycloak.migration.usersExportStrategy=REALM_FILE \
    -Dkeycloak.migration.file=/opt/jboss/keycloak/standalone/data/myrealm_bak.json

docker run \
  --name keycloak \
  --rm \
  -p 8080:8080 \
  -p 8443:8443 \
  -v $(pwd)/backup/myrealm_bak.json:/opt/jboss/keycloak/standalone/data/myrealm_bak.json \
  --network=keycloak-net \
  -e KEYCLOAK_USER=admin \
  -e KEYCLOAK_PASSWORD=admin \
  quay.io/keycloak/keycloak:10.0.2
```
