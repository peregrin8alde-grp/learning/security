# Keycloak Quickstarts

https://github.com/keycloak/keycloak-quickstarts

```
git clone --depth 1 https://github.com/keycloak/keycloak-quickstarts.git
```

## service-jee-jaxrs

### System Requirements

- WildFly 10
- Java 8.0 (Java SDK 1.8) or later
- Maven 3.3.3 or later

### Configuration in Keycloak

http://localhost:8080/auth/admin

- Settings
  - Client ID: `service-jaxrs`
  - Client Protocol: `openid-connect`
  - Access Type: `bearer-only`
- Installation
  1. `Keycloak OIDC JSON`
  2. Click Download
  3. Move the file keycloak.json to the config/ directory in the root of the quickstart
- CORS の有効化
  ```
  {
  ...
  "enable-cors": true
  }
  ```

### Build and Deploy the Quickstart

#### デプロイ先サーバ準備

https://hub.docker.com/r/jboss/wildfly/

```
docker run \
  --name wildfy \
  --rm \
  -it \
  -p 18080:8080 \
  -p 9990:9990 \
  --network=keycloak-net \
  -v wildfly:/opt/jboss/wildfly \
  jboss/wildfly \
    /opt/jboss/wildfly/bin/standalone.sh \
      -b 0.0.0.0 \
      -bmanagement 0.0.0.0
```

https://docs.wildfly.org/20/Getting_Started_Guide.html#installation

```
docker exec -it wildfy wildfly/bin/add-user.sh

a
admin
admin
```

http://localhost:18080

#### アダプタインストール

https://www.keycloak.org/docs/latest/securing_apps/index.html#jboss-eap-wildfly-adapter

```
docker exec -it wildfy bash

cd wildfly
curl -LO https://downloads.jboss.org/keycloak/10.0.2/adapters/keycloak-oidc/keycloak-wildfly-adapter-dist-10.0.2.tar.gz
tar zxvf keycloak-wildfly-adapter-dist-10.0.2.tar.gz

./bin/jboss-cli.sh --file=bin/adapter-elytron-install-offline.cli
```

サーバ再起動

#### ビルド／デプロイ

https://hub.docker.com/_/maven

```
docker run \
  -it \
  --rm \
  -v maven-repo:/root/.m2 \
  -v "$(pwd)"/keycloak-quickstarts:/usr/src/mymaven \
  -w /usr/src/mymaven/service-jee-jaxrs \
  --network=keycloak-net \
  maven:3.6.3-jdk-8 \
    mvn clean -Dwildfly.hostname="wildfy" wildfly:deploy
```

admin / admin

#### 動作確認

- public - http://localhost:18080/service/public
- secured - http://localhost:18080/service/secured
- admin - http://localhost:18080/service/admin

## app-jee-html5

https://github.com/keycloak/keycloak-quickstarts/tree/latest/app-jee-html5

### System Requirements

- WildFly 10
- Java 8.0 (Java SDK 1.8) or later
- Maven 3.3.3 or later
- service-jee-jaxrs 起動サーバ
  - `http://localhost:8080/service` から変更している場合、`app.js` の `serviceUrl` を修正
  - 上記の通り、`http://localhost:18080/service`

### Configuration in Keycloak

http://localhost:8080/auth/admin

- Settings
  - Client ID: `app-html5`
  - Client Protocol: `openid-connect`
  - Root URL: `http://localhost:18080/app-html5`
  - Access Type: `public`
- Installation
  1. `Keycloak OIDC JSON`
  2. Click Download
  3. Move the file keycloak.json to the config/ directory in the root of the quickstart

### Build and Deploy the Quickstart

wildfly はサーバ側と共通とする。

#### ビルド／デプロイ

```
docker run \
  -it \
  --rm \
  -v maven-repo:/root/.m2 \
  -v "$(pwd)"/keycloak-quickstarts:/usr/src/mymaven \
  -w /usr/src/mymaven/app-jee-html5 \
  --network=keycloak-net \
  maven:3.6.3-jdk-8 \
    mvn clean -Dwildfly.hostname="wildfy" wildfly:deploy
```

admin / admin

http://localhost:18080/app-html5
