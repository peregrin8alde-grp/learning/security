# OpenID Connect

OAuth2 の拡張

- https://openid.net/
- https://openid.net/developers/specs/

## 概要

- https://openid.net/specs/openid-connect-core-1_0.html
- 1.3. Overview

1. The RP (Client) sends a request to the OpenID Provider (OP).
   - ユーザのリソースにアクセスしたいアプリ（RP）が OP に対し要求
1. The OP authenticates the End-User and obtains authorization.
   - OP はユーザにアクセス許可するかどうか確認（認可）とユーザ認証を実施
   - 許可する場合、ユーザはログインを実施
1. The OP responds with an ID Token and usually an Access Token.
   - OP はユーザ識別情報とリソースアクセス用のトークンを発行して RP に返信
1. The RP can send a request with the Access Token to the UserInfo Endpoint.
   - RP はアクセストークンを使って UserInfo（ユーザ情報）を要求
1. The UserInfo Endpoint returns Claims about the End-User.
   - OP は UserInfo（ユーザ情報）を RP に返信

```
+--------+                                   +--------+
|        |                                   |        |
|        |---------(1) AuthN Request-------->|        |
|        |                                   |        |
|        |  +--------+                       |        |
|        |  |        |                       |        |
|        |  |  End-  |<--(2) AuthN & AuthZ-->|        |
|        |  |  User  |                       |        |
|   RP   |  |        |                       |   OP   |
|        |  +--------+                       |        |
|        |                                   |        |
|        |<--------(3) AuthN Response--------|        |
|        |                                   |        |
|        |---------(4) UserInfo Request----->|        |
|        |                                   |        |
|        |<--------(5) UserInfo Response-----|        |
|        |                                   |        |
+--------+                                   +--------+
```
